#!/bin/bash
yum install {{ web_service_name }} -y
yum update -y
chkconfig {{ web_service_name }} on
service {{ web_service_name }} start