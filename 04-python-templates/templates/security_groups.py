#!/usr/bin/env python
from troposphere import Output, Parameter, Ref
from troposphere.ec2 import SecurityGroup, SecurityGroupRule
from sceptre_template import SceptreTemplate

class SceptreResource(SceptreTemplate):
    def __init__(self, sceptre_user_data):
        super(SceptreResource, self).__init__()
        self.sceptre_user_data = sceptre_user_data
        self.add_parameters()
        self.add_security_groups()

    def add_parameters(self):
        self.vpc_id = self.template.add_parameter(Parameter(
            "BlueVpcId",
            Type="String"
        ))

    def add_security_groups(self):
        self.security_group_dictionary = {}
        for sg in self.sceptre_user_data["SecurityGroups"]:
            sg_name = list(sg.keys())[0]
            sg_kwargs = {
                "VpcId": Ref(self.vpc_id),
                "GroupName": sg_name
            }
            sg_kwargs.update(sg[sg_name])
            if sg_kwargs.get("SecurityGroupEgress"):
                sg_kwargs.update(self.add_rules(sg_kwargs["SecurityGroupEgress"], "Egress"))
            if sg_kwargs.get("SecurityGroupIngress"):
                sg_kwargs.update(self.add_rules(sg_kwargs["SecurityGroupIngress"], "Ingress"))
            if sg_kwargs.get("Tags"):
                sg_kwargs.update(self.tag_resource(sg_kwargs.pop("Tags")))
            sg = self.template.add_resource(SecurityGroup(sg_name, **sg_kwargs))
            self.security_group_dictionary[sg_name] = sg
            self.template.add_output(Output(
                sg_name + "Id",
                Value=Ref(sg)
            ))

    def add_rules(self, rules, ingress_or_egress):
        rules_list = []
        if ingress_or_egress == "Ingress":
            sg_id_type = "SourceSecurityGroupId"
        else:
            sg_id_type = "DestinationSecurityGroupId"

        for i, rule in enumerate(rules):
            if not rule.get("IpProtocol"):
                rule["IpProtocol"] = "tcp"
            if rule.get("Port"):
                rule_port = rule.pop("Port")
                rule.update({
                    "FromPort": rule_port,
                    "ToPort": rule_port
                })
            elif rule.get("Ports"):
                port_range = rule.pop("Ports").replace(" ", "")
                rule_from_port = port_range.split('-')[0]
                rule_to_port = port_range.split('-')[1]
                rule.update({
                    "FromPort": rule_from_port,
                    "ToPort": rule_to_port
                })
            if rule.get(sg_id_type) in self.security_group_dictionary:
                rule[sg_id_type] = Ref(self.security_group_dictionary[rule.get(sg_id_type)])
            rules_list.append(SecurityGroupRule("Rule" + str(i), **rule))
        return {"SecurityGroup" + ingress_or_egress: rules_list}

def sceptre_handler(sceptre_user_data):
    security_groups = SceptreResource(sceptre_user_data)
    return security_groups.template.to_json()
