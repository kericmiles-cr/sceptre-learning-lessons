from troposphere import Ref, Select, Parameter, Output, Base64, Join
from troposphere.ec2 import Instance

from sceptre_template import SceptreTemplate


class SceptreResource(SceptreTemplate):
    def __init__(self, sceptre_user_data):
        super(SceptreResource, self).__init__()
        self.sceptre_user_data = sceptre_user_data
        self.add_parameters()
        self.add_instances()

    def add_parameters(self):
        self.sg_ids = self.template.add_parameter(Parameter(
            "SecurityGroupIds",
            Type="List<AWS::EC2::SecurityGroup::Id>"
        ))
        self.subnet_ids = self.template.add_parameter(Parameter(
            "SubnetIds",
            Type="List<AWS::EC2::Subnet::Id>"
        ))

    def add_instances(self):
        number_of_instances = self.sceptre_user_data.pop("NumberOfInstances")
        common_tags = self.sceptre_user_data.pop("CommonTags")
        instance_specific_tags = self.sceptre_user_data.pop("InstanceSpecificTags")
        for i in xrange(0, number_of_instances):
            instance_kwargs = {
                "SecurityGroupIds": Ref(self.sg_ids),
                "SubnetId": Select(i, Ref(self.subnet_ids))
            }
            if "UserData" in self.sceptre_user_data:
                user_data = self.read_from_file(self.sceptre_user_data["UserData"]["Path"], self.sceptre_user_data["UserData"].get("Variables", {}))
                instance_kwargs.update({"UserData": user_data})
            instance_kwargs.update(self.sceptre_user_data["InstanceProperties"])
            instance_tags = instance_specific_tags.get("Instance" + str(i + 1))
            if instance_tags:
                instance_tags.update(common_tags)
            else:
                instance_tags = common_tags
            instance_kwargs.update(self.tag_resource(instance_tags))
            instance = self.template.add_resource(Instance("Instance" + str(i + 1), **instance_kwargs))
            self.template.add_output(Output(
                "Instance" + str(i + 1),
                Value=Ref(instance)
            ))

def sceptre_handler(sceptre_user_data):
    """Get template with Cloudformation resources"""
    return SceptreResource(sceptre_user_data).template.to_json()
