from troposphere import Template, Tags, Base64, Join
from jinja2 import Environment, FileSystemLoader
import os

class SceptreTemplate(object):
    """This parent class exists in order to encapsulate functions that are
    commonly-used across various Sceptre templates.
    """
    #instantiate the SceptreTeamplate object as a troposphere temaplate
    def __init__(self):
        self.template = Template()
    #function to handle tagging provided by a dictionary or no tags if nothing is passed
    def tag_resource(self, tag_dict):
        if tag_dict:
            return {"Tags": Tags(**tag_dict)}
        else:
            return {"Tags": Tags()}
    #read userdata from a file and encode it to be used in a CloudFormation template (Base64)
    def read_from_file(self, file_path, variables):
        data = []

        path, filename = os.path.split(file_path)
        render_result = Environment(
            loader=FileSystemLoader(path)
        ).get_template(filename).render(variables)

        userdata_filepath = os.path.join(path, ".rendered_" + filename)
        with open(userdata_filepath, 'w') as userdata:
            userdata.write(render_result)

        with open(userdata_filepath, 'r') as userdata:
            for line in userdata:
                data.append(line)
        os.remove(userdata_filepath)
        return Base64(Join("", data))
